Randomized Dystopia
===================

[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

I see a lot of repetitive dystopian fiction about denying people (often teenagers) the right to free speech or freedom of movement. But did you know that The Universal Declaration of Human Rights, The Convention on the Rights of the Child, and The Convention on the Elimination of All Forms of Discrimination against Women discuss several more ways that governments sometimes stomp on our liberties? Here are several!

This application -- running at [http://www.harihareswara.net/dystopia/](http://www.harihareswara.net/dystopia/) --  displays a few random rights to the viewer, for use in constructing
different dystopian YA novels. It's a Flask application very similar to
[my silly scifi novel titles](https://github.com/brainwane/novel-titles) app. The content's in the lists of dictionaries within articles.py.

I wrote more about why I made it [at my director's commentary](http://www.harihareswara.net/sumana/2015/03/24/0).

To use or reuse
===============
Go ahead! Remix and reuse -- the code is under the GPL and the United Nations material
belongs to its respective rightsholders but I figure this is fine.

Reminder to Sumana: to restart the app on production, touch dystopia.wsgi.

